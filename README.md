# STAPL Home Page

This is the HTML for the landing page for STAPL.

## Thanks

* This page is based on the Air template from
  [FreeHTML5.co](https://freehtml5.co).
* The photo of Texas Bluebonnets is from
  [Unsplash](https://unsplash.com/photos/YPLx5eOy49M).
