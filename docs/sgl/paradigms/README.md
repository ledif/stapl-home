# Other Paradigms
SGL also provides three other paradigms (apart from the KLA paradigm) to improve performance and allow greater expressivity. These include:

## Fine-grained paradigms:
* k-Level Asynchronous (KLA) -- described above.
* Hybrid Out-of-Core Paradigm -- to allow efficient processing of large graphs that do not fit in RAM, without penalty.
* Hierarchical Communication Reduction Paradigm -- to allow fast processing of power-law graphs.

All fine-grained algorithms reuse the same vertex and neighbor operators, thereby allowing users to easily swap between the paradigms based on use-case. The operators are described in detail in the previous sections.

## Coarse-grained paradigm:
* Hierarchical Paradigm -- to allow expressing of naturally hierarchical graph algorithms.

We describe each of these paradigms in detail below, as well as give qualitative advice on when to use them.
