# STAPL Graph Library

The STAPL Graph Library is a distributed-memory high-performance parallel
graph processing framework written in C++ using STAPL. Its backend can be
configured to use MPI, OpenMP or (by default) a combination of both.

## Getting Started

Take a look at the [Getting Started](../getting-started.md) guide to learn the basics
about building and running STAPL. Then check out the [tutorial](basic-usage/README.md)
for a brief walkthrough of a basic SGL program.

## People

The PIs for this work are [Nancy M. Amato](http://parasol.tamu.edu/~amato) and
[Lawrence Rauchwerger](http://parasol.tamu.edu/~rwerger).

The following people have been the lead contributors for this software:
* Gabriel Tanase (<igtanase@us.ibm.com>)
* Harshvardhan (<ananvay@google.com>)
* Adam Fidel (<adamfidel@tamu.edu>)

This work has been created with the help of numerous students, post-docs and
research staff including (but not limited to!) Timmie Smith, Nathan Thomas,
Antal Buss, Brandon West, Jeremy Vu, Mani Zandifar, Alireza Majidi, Glen
Hordemann, Ioannis Papadopolous.

The current maintainer of the framework is [Adam Fidel](https://ledif.me).

## Publications

The following articles have been written using the STAPL Graph Library:

* Adam Fidel, Francisco Coral Sabido, Colton Riedel, Nancy Amato, Lawrence Rauchwerger, "Fast Approximate Distance Queries in Unweighted Graphs using Bounded Asynchrony", To appear in Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Rochester, NY, USA, September 2016.

* **Best Paper Finalist.** Harshvardhan, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "An Algorithmic Approach to Communication Reduction in Parallel Graph Algorithms," In Proc. IEEE Int.Conf. on Parallel Architectures and Compilation Techniques (PACT), San Francisco, CA, USA, November 2015.

* Ioannis Papadopoulos, Nathan Thomas, Adam Fidel, Dielli Hoxha, Nancy M. Amato, Lawrence Rauchwerger, "Asynchronous Nested Parallelism for Dynamic Applications in Distributed Memory," In Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Raleigh, NC, USA, September 2015.

* Ioannis Papadopoulos, Nathan Thomas, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "STAPL-RTS: An Application Driven Runtime System," In International Conference on Supercomputing (ICS), Newport Beach, California, USA, June 2015.

* Harshvardhan, Brandon West, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "A Hybrid Approach To Processing Big Data Graphs on Memory-Restricted Systems," In Proc. Int. Par. and Dist. Proc. Symp. (IPDPS), Hyderabad, India, May 2015.

*  **Best Paper Award.** Harshvardhan, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "KLA: A New Algorithmic Paradigm for Parallel Graph Computations," In Proc. IEEE Int.Conf. on Parallel Architectures and Compilation Techniques (PACT), Aug 2014.

* Adam Fidel, Sam Ade Jacobs, Shishir Sharma, Nancy M. Amato, Lawrence Rauchwerger, "Using Load Balancing to Scalably Parallelize Sampling-Based Motion Planning Algorithms," In Proc. Int. Par. and Dist. Proc. Symp. (IPDPS), Phoenix, Arizona, USA, May 2014. (pdf, abstract)

* Harshvardhan, Adam Fidel, Nancy M. Amato, Lawrence Rauchwerger, "The STAPL Parallel Graph Library," In Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Tokyo, Japan, Sep 2012.

* Gabriel Tanase, Antal Buss, Adam Fidel, Harshvardhan, Ioannis Papadopoulos, Olga Pearce, Timmie Smith, Nathan Thomas, Xiabing Xu, Nedhal Mourad, Jeremy Vu, Mauro Bianco, Nancy M. Amato, Lawrence Rauchwerger, "The STAPL Parallel Container Framework," In Proc. ACM SIGPLAN Symp. Prin. Prac. Par. Prog. (PPOPP), Feb 2011.
