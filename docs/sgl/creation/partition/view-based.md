# View-Based

In addition to the other partitioning methods, SGL provides a view-based
distribution mechanism. For example, one can create a graph that is distributed
block-cyclically:

```c++
using spec_type = stapl::distribution_spec<>;

using graph_type = stapl::dynamic_graph<
  stapl::DIRECTED,
  stapl::MULTIEDGES,
  int,
  int,
  stapl::view_based_partition<spec_type>,
  stapl::view_based_mapper<spec_type>
>;  

const std::size_t n = 100;
auto cyclic_spec = stapl::block_cyclic(n, 4);

graph_type g(cyclic_spec);
```

Using this code, `g` will be distributed cyclically across the locations in
block sizes of 4.
