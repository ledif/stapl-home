# Manual Graph Manipulation

A graph can have edges added to or deleted from it. A `dynamic_graph`, in addition, can also have vertices added to or deleted from it. Edges can be added by specifying the source and target vertices of the edge in the graph:

```c++
g.add_edge(source_descriptor, target_descriptor[, edge_property]);
```

For example, the following will add an edge between vertices 3 and 11 in the graph:

```c++
g.add_edge(3, 11);
```

If edges are being added in bulk, it is typically faster to added them asynchronously. However, a synchronization is needed before using the graph after the bulk edge-addition is done:

```c++
for (size_t i=0; i<n; ++i) {
  g.add_edge_async(i, i+1 % n);  // Add edges asynchronously.
}
rmi_fence();  // Synchronize.
// ...Now we can use the graph in algorithms, etc.
```

Adding vertices in dynamic graphs is also similar, and requires a synchronization before the graph can be used:

```c++
g.add_vertex(vertex_descriptor, [vertex_property]);
```

The following adds a vertex with descriptor `575` with the property (string) `"foobar"`:

```c++
dynamic_graph<DIRECTED, MULTIEDGES, string, int> d;
d.add_vertex(575, “foobar”);
rmi_fence();  // synchronize.
```
