# make_grid

Defined in <`stapl/containers/graph/generators/grid.hpp`>

```c++
template<typename GraphView,unsigned long D>  
GraphView make_grid(std::array< std::size_t, D > dims,bool bidirectional)
```

## Summary

Generates a multidimensional grid graph.

The generated graph will be a hyperrectangular grid in n-dimensions. In 2D, it will create a 2D mesh. In 3D, it will create a cube and so on. The returned view owns its underlying container.


#### Parameters
* *dims*: An array of sizes in each dimension 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  std::array<std::size_t, 3> dims{{8, 16, 8}};

  auto v = stapl::generators::make_grid<view_type>(dims, true);
```
