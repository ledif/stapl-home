# make_list

Defined in <`stapl/containers/graph/generators/list.hpp`>

```c++
template<typename GraphView>  
GraphView make_list(size_t n,bool bidirectional)
```

## Summary

Generate a list graph.

The generated list will contain n vertices, each with an edge to its next element in the list (except for the last).

The returned view owns its underlying container.


#### Parameters
* *n*: The number of nodes in the list. 


* *bidirectional*: True for adding back-edges, False for forward only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_list<view_type>(1024);
```
