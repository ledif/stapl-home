# make_watts_strogatz

Defined in <`stapl/containers/graph/generators/watts_strogatz.hpp`>

```c++
template<typename GraphView>  
GraphView make_watts_strogatz(size_t n,size_t k,double p,bool bidirectional,size_t max_procs)
```

## Summary

Generate a Watts-Strogatz small-world graph [^1].

First create a ring over n nodes. Then each node in the ring is connected with its k nearest neighbors (k-1 neighbors if k is odd). Then shortcuts are created by replacing some edges as follows: for each edge u-v in the underlying "n-ring with k nearest neighbors" with probability p replace it with a new edge u-w with uniformly random choice of existing node w.

In contrast with Newman-Watts-Strogatz, the random rewiring does not increase the number of edges. The rewired graph is not guaranteed to be connected.

The returned view owns its underlying container.


#### Parameters
* *n*: The number of nodes in the graph 


* *k*: Each node is connected to k nearest neighbors in ring topology 


* *p*: The probability of rewiring each edge 


* *bidirectional*: True for adding back-edges, False for forward only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_watts_strogatz<view_type>(64, 8, 0.2, true);
```
 [^1] Duncan J. Watts and Steven H. Strogatz, Collective dynamics of small-world networks, Nature, 393, pp. 440442, 1998.
