# make_binary_tree_network

Defined in <`stapl/containers/graph/generators/binary_tree_network.hpp`>

```c++
template<typename GraphView>  
GraphView make_binary_tree_network(size_t num_levels)
```

## Summary

Generate a binary tree network.

The binary tree network will contain 2*num_levels levels. Network consists of two num_level binary trees which share the same leaves. The root of the first tree forms the source, and the root of the second forms the sink of the network.

The returned view owns its underlying container.

The type of the graph needs to be dynamic.


#### Parameters
* *num_levels*: Number of levels in each of the 2 generated trees. 





#### Returns
A view over the generated graph.


![https://i.imgur.com/jUtBdpa.png](https://i.imgur.com/jUtBdpa.png)

#### Example
```cpp
  using graph_type = stapl::dynamic_graph<stapl::DIRECTED, stapl::MULTIEDGES>;
  using view_type = stapl::graph_view<graph_type>;

  auto v = stapl::generators::make_binary_tree_network<view_type>(4);
```
