# make_torus

Defined in <`stapl/containers/graph/generators/torus.hpp`>

```c++
template<typename GraphView>  
GraphView make_torus(size_t nx,size_t ny,bool bidirectional)
```

## Summary

Generates an x-by-y torus.

The generated torus will have x vertices in the horizontal direction and y vertices in the vertical direction. This is distributed n/p in the y-direction.

The returned view owns its underlying container.


#### Parameters
* *nx*: Size of the x-dimension of the torus. 


* *ny*: Size of the y-dimension of the torus. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_torus<view_type>(16, 16, true);
```
