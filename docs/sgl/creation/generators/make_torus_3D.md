# make_torus_3D

Defined in <`stapl/containers/graph/generators/torus_3D.hpp`>

```c++
template<typename GraphView>  
GraphView make_torus_3D(size_t nx,size_t ny,size_t nz,bool bidirectional)
```

## Summary

Generates an x-by-y-by-z 3D torus.

The generated torus will have x vertices in the horizontal direction, y vertices in the vertical direction, and z vertices in the in-out direction. This is distributed n/p in the z-direction, where n = x*y*z.

The returned view owns its underlying container.


#### Parameters
* *nx*: Size of the x-dimension of the torus. 


* *ny*: Size of the y-dimension of the torus. 


* *nz*: Size of the z-dimension of the torus. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_torus_3D<view_type>(16, 8, 16, true);
```
