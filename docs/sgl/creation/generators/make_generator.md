# make_generator

Defined in <`stapl/containers/graph/generators/generator.hpp`>

```c++
template<typename GraphView,typename EF>  
[generator](#structstapl_1_1generators_1_1generator)< GraphView, EF > make_generator(size_t num_vertices,EF const & edge_gen)
```

## Summary



#### Parameters
* *num_vertices*: Number of vertices in the generated graph. 


* *edge_gen*: A functor which is called to generate the edges. 





#### Returns
A generator object which will generate the desired graph upon invocation.


The generator object will return a view which owns its underlying container. 
**See also**: make_generator
