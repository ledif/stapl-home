# Generators

SGL provides a framework for defining graph-generators -- rules that allow generation of graphs -- such generators can be used to generate a variety of large-scale input graphs quickly and efficiently. Using this framework, SGL provides a variety of graph generators as
outlined in this section.

Users may choose to define their own rules for graph generation, by providing an edge-connector operator to our graph_generator framework. This operator takes as input a vertex v and a GraphView g, and adds to the view edges that should start from the vertex v.

Here is a simple way to use one of the predefined generators to create an input torus graph. Note that the input parameters to different generators will vary, and specify different things based on the semantics of that generator.

```c++
using graph_type = dynamic_graph<UNDIRECTED, MULTIEDGES>;
using view_type = graph_view<graph_type>;

// Generate vertices and edges in graph in form of a 2-D toroid
// with dimensions 2000 x 50
int x = 2000, y = 50;
view_type torus_graph = generators::make_torus<view_type>(x, y);
```
