# make_barabasi_albert

Defined in <`stapl/containers/graph/generators/barabasi_albert.hpp`>

```c++
template<typename GraphView>  
GraphView make_barabasi_albert(const std::size_t n,const std::size_t d,bool add_reverse_edge)
```

## Summary

Generate a scale-free network based on the Barabasi-Albert preferential attachment model.

This implementation is based on the following paper:

Vertices with lower vertex IDs are more likely to be the target for edges. Note that if add_reverse_edge is false, the outgoing edge distribution is constant, as each vertex will have exactly 'd' edges.

The returned view owns its underlying container.


#### Parameters
* *n*: Number of vertices in the generated graph. 


* *d*: The number of outgoing edges to generate for each vertex 





#### Returns
A view over the generated graph.


![https://i.imgur.com/2nwNn5l.png](https://i.imgur.com/2nwNn5l.png)

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_binary_tree<view_type>(256);
```
 [^1] Scalable Generation of Scale-free Graphs. Peter Sanders, Christian Schulz. arXiv:1602.07106v1. [cs.DS] 23 Feb 2016. [http://arxiv.org/pdf/1602.07106v1.pdf](http://arxiv.org/pdf/1602.07106v1.pdf) [foo]:
