# make_sparse_mesh

Defined in <`stapl/containers/graph/generators/sparse_mesh.hpp`>

```c++
template<typename GraphView>  
GraphView make_sparse_mesh(size_t nx,size_t ny,double p,bool bidirectional)
```

## Summary

Generates an x-by-y sparse mesh.

The generated sparse mesh will contain x vertices in the horizontal direction and y vertices in the vertical direction. This is distributed n/p in the y-direction.

The returned view owns its underlying container.

Each edge is added with a probability of p. Thus, p = 1 generates a complete mesh, and p = 0.5 generates a mesh with 50% of the edges of a complete mesh. 
#### Parameters
* *nx*: Size of the x-dimension of the sparse mesh. 


* *ny*: Size of the y-dimension of the sparse mesh. 


* *p*: Probability of adding a particular edge to the mesh. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_sparse_mesh<view_type>(16, 16, 0.5, true);
```
