# make_unconnected_cycles

Defined in <`stapl/containers/graph/generators/cycles.hpp`>

```c++
template<typename GraphView>  
GraphView make_unconnected_cycles(size_t cycle_count,size_t cycle_size,bool bidirectional)
```

## Summary

Generates a graph containing many unconnected cycles.

The generated graph will contain cycle_count cycles, each of which has cycle_size vertices. The cycles are not connected.

Results in cycle_count SCCs/WCCs in a directed graph. Results in cycle_count CCs in an undirected graph.

The returned view owns its underlying container.


#### Parameters
* *cycle_count*: Number of cycles in the generated graph. 


* *cycle_size*: Number of vertices in each cycle. 


* *bidirectional*: True to add back-edges in a directed graph, false for forward edges only. 





#### Returns
A view over the generated graph.

#### Example
```cpp
  using view_type = stapl::graph_view<stapl::multidigraph<int>>;

  auto v = stapl::generators::make_watts_strogatz<view_type>(16, 8, 0.2, true);
```
