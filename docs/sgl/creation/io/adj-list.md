# Adjacency List

SGL provides facilities to read and write the common adjacency list format.

## Format

The first line of the input is the size of the graph in terms of the number of
vertices and the number of edges.
Every line after that will contain the adjacency list for a single vertex `source target0 target1 ...`. For example,
an unweighted graph with 3 vertices and 4 edges will look like:

```
3 4
0 1
1 2
2 0 1
```

## Reading

To read an adjacency list from a file, call the function as follows:

```c++
graph_view<Graph> read_adj_list(std::string filename, size_t blk_sz=4096);
```

## Writing

To write a graph to a file in the adjacency list format, use:

```c++
write_adj_list(G g, std::string filename);
```
