# approximate_breadth_first_search

Defined in <`stapl/containers/graph/algorithms/approx_breadth_first_search.hpp`>

```c++
template<class GView>  
size_t approximate_breadth_first_search(GView & g,typename GView::vertex_descriptor const & source,size_t k,double tau)
```

## Summary

Builds an approximate breadth-first search tree for a graph. [^1].

Performs a breadth-first search on the input graph_view, storing the BFS-level and BFS-parent on each reachable vertex. The BFS-level stored is at most k times the true BFS-level. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *source*: The descriptor of the source vertex for this traversal. 


* *k*: The maximum amount of asynchrony allowed in each phase. 


* *tau*: A parameter that controls the amount of error allowed upon a visitation. 





#### Returns
The number of iterations performed by the paradigm.


**See also**: [properties::approximate_bfs_property](#classstapl_1_1properties_1_1approximate__bfs__property)

#### Example
```cpp
  using property_type = stapl::properties::approximate_bfs_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);

  stapl::approximate_breadth_first_search(g, 0, 10, 0.1);

  std::size_t hops = g[10].property().level();

  std::cout << "vertex 10 is roughly " << hops << " hops away from vertex 0";
```
 [^1] Adam Fidel, Francisco Coral, Colton Riedel, Nancy M. Amato, Lawrence Rauchwerger, "Fast Approximate Distance Queries in Unweighted
     Graphs using Bounded Asynchrony," In Wkshp. on Lang. and Comp. for Par. Comp. (LCPC), Sep 2016.
