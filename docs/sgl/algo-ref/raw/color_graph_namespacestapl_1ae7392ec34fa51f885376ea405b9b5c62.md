# color_graph

Defined in <`stapl/containers/graph/algorithms/graph_coloring.hpp`>

```c++
template<template< typename, typename, typename, typename > class GView,template< graph_attributes, graph_attributes, typename...> class Graph,graph_attributes M,typename... OptionalParams,typename Dist,typename DomMap,typename MapFunc,typename Derived,typename ColorMap>  
void color_graph(GView< Graph< UNDIRECTED, M, OptionalParams...>, iterator_domain< Dist, DomMap >, MapFunc, Derived > const & graph,ColorMap & color_map)
```

## Summary

Parallel Level-Synchronized Graph Coloring Algorithm.

Assigns coloring to the vertices of the input graph, such that neighboring vertices are not assigned the same color. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *color_map*: The output vertex property map where the color information will be stored. [vertex->color (int)] Specialization for iterator_domain based graph_view over dynamic_graph, because sparse-domain is needed in the algorithm to have a view over the boundary vertices.
