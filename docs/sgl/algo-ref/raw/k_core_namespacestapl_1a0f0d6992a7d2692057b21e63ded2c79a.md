# k_core

Defined in <`stapl/containers/graph/algorithms/k_core.hpp`>

```c++
template<typename GView>  
size_t k_core(GView & graph,int core_sz,size_t k)
```

## Summary

A Parallel k-core algorithm.

A variant of [k_core()](#namespacestapl_1a34d0c71a894e6f12a2c423758839648a) that uses the less<int>() comparator. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *core_sz*: The parameter to control which vertices are deleted. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous paradigm. k >= D implies fully asynchronous paradigm (D is diameter of graph). 





#### Returns
The number of iterations performed by the paradigm. Note this is different from the dynamic version which returns the number of vertices deleted.
