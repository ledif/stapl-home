# pscc

Defined in <`stapl/containers/graph/algorithms/pscc_kla.hpp`>

```c++
template<typename GraphView>  
void pscc(GraphView & g,size_t k,float degree_percentage,double pivot_count)
```

## Summary

SCCMulti algorithm for identifying Strongly Connected Components (SCCs).

This function executes the SCCMulti algorithm on the input graph. It begins by selecting approximately pivot_count pivots and then adaptively selects a new value on each iteration. The SCC color is stored in the property using a set_cc() function on the graph's vertex property. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony for kla. 


* *degree_percentage*: The threshold for a vertex's degree after which it will be considered a hub (given as a percentage of total graph size). 


* *pivot_count*: The number of pivots to select at each iteration.
