# is_same_cc

Defined in <`stapl/containers/graph/algorithms/connected_components.hpp`>

```c++
template<typename GView>  
bool is_same_cc(GView & g,typename GView::vertex_descriptor v1,typename GView::vertex_descriptor v2)
```

## Summary

Algorithm to compute if two specified vertices belong to the same connected component (i.e., are connected through a path), based on the given input graph and the Connected-Component IDs of vertices.

#### Parameters
* *g*: The graph_view over the input graph. property_type on the vertex needs to have methods: vertex_descriptor cc(void)  for getting the connected-component id. cc(vertex_descriptor)  for setting the connected-component id. The connected-component for each vertex must be available, or be filled-in by calling connected_components(g) algorithm before calling is_same_cc. 


* *v1*: Vertex descriptor of the first vertex. 


* *v2*: Vertex descriptor of the second vertex. 





#### Returns
True if both vertices are in the same Connected-Component, or false otherwise.
