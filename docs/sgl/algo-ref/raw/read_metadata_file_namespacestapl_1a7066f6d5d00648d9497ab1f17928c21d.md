# read_metadata_file

Defined in <`stapl/containers/graph/algorithms/sharded_graph_io.hpp`>

```c++
[sharded_description](#structstapl_1_1sharded__description) read_metadata_file(std::string const & filename)
```

## Summary

Read the metadata file from disk and create a description of how the input graph is sharded.

This is a collective operation. One location will read the file and broadcast its contents to all other locations. 


#### Parameters
* *filename*: Path to the metadata file 





#### Returns
Description of the sharding
