# level_sync_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/level_sync_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename PostExecute,typename GView,typename Predicate>  
size_t level_sync_paradigm(WF const & uwf,UF const &,PostExecute post_execute,GView & g,Predicate pred)
```

## Summary

The Parallel Level-Synchronous (level_sync) Paradigm.

Implements the level-sync paradigm, which iteratively executes BSP-Supersteps (SS). Each SS applies the user provided work function over the vertices of the input graph. Any communication generated within a SS is guaranteed to have finished before the SS finishes. The user provides a vertex-operator to express the computation to be performed on each vertex, and a neighbor-operator that will be applied to each neighbor that is visited. The vertex-operator is passed in a vertex and a visit object. To visit a neighboring vertex, the vertex-operator must call visit(neighbor, neighbor-operator()). The vertex-operator must return true if the vertex was active (i.e. its value was updated), or false otherwise. The neighbor-operator is passed in the target vertex. Neighbor-operators may carry state, but must be immutable. They should return true if the visit was successful (i.e. the target vertex will be activated after this visit), or false otherwise. Users may also provide additional functions to be executed before and after each SS. 
#### Parameters
* *WF*: The type of the user provided vertex-operator expressing computation to be performed over each vertex. 


* *UF*: The type of the user provided neighbor-operator expressing computation to be performed over neighboring vertices. 


* *Predicate*: a predicate which recieves a vertex and decides whether or not to terminate. The paradigm defaults to a false_predicate. 





#### Parameters
* *uwf*: Functor that implements the operation to be performed on each vertex 


* *post_execute*: Optional functor that will be executed on the graph_view at the end of each SS. This will be invoked with the input graph_view and the current SS ID (the ID of the SS that just finished). 


* *g*: The graph_view over the input graph. 





#### Returns
The number of iterations performed by the paradigm.
