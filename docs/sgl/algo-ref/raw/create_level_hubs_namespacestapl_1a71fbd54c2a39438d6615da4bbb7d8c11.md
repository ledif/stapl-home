# create_level_hubs

Defined in <`stapl/containers/graph/algorithms/create_level_hubs.hpp`>

```c++
template<typename GraphView>  
graph_view< digraph< [create_level_hubs_detail::super_vertex_hub_property](#structstapl_1_1create__level__hubs__detail_1_1super__vertex__hub__property)< GraphView > > > create_level_hubs(GraphView & gvw,size_t k,bool delete_edges)
```

## Summary

Algorithm to create a level of hierarchy based on hubs, for use in [h_hubs_paradigm()](#namespacestapl_1a8d958fad783ec030aa95c560587efcc7).

Creates a level of hierarchy based on the input graph_view and high-degree vertices (hubs) in the input graph, i.e., all hub vertices will have hub-vertex representative supervertices on each location. Supervertices and superedges in the resulting output graph store descriptors of their children in the input.

For use with the [h_hubs_paradigm()](#namespacestapl_1a8d958fad783ec030aa95c560587efcc7), this must be called before calling [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb) on the input graph.

The algorithm first finds all vertices qualifying as hubs (i.e. have out-degree greater than the given threshold (`k`). A new level of hierarchy is created with one supervertex per location, and each such supervertex contains representatives of all hub vertices. These representatives can only be written to, and their values can be flushed to the actual hub-vertices in the input graph.

This incurs O(h) storage per location, where h is the number of hubs, as defined by the hub cutoff `k`.


#### Parameters
* *gvw*: The input graph_view over which the new level is to be created. 


* *k*: The cutoff degree for considering a vertex as a hub. All vertices with out-degree greater than or equal to k are considered hub vertices. 


* *delete_edges*: Indicates if the non-local edges for the input graph will be deleted. This is true by default, as the [h_paradigm](#namespacestapl_1ab2f2aaf9b13ecd411395eae6d992804b) requires the edges to be deleted. 





#### Returns
A graph_view over the output supergraph. 

Destructive, the input graph view will be mutated to delete some or all edges, but the information of the graph is maintained between the input and the hierarchy.
