# write_edge_list

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename GraphVw>  
void write_edge_list(GraphVw & g,std::string filename)
```

## Summary

Function to output the edge-list of a graph, without properties.

Format: /// SRC0 TGT0
/// SRC1 TGT1
/// SRC2 TGT2
/// :
/// 
#### Parameters
* *g*: The input graph_view to be printed. 


* *filename*: The name of the file where the graph will be printed.
