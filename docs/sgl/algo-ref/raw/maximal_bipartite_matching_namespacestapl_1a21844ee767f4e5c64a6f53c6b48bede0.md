# maximal_bipartite_matching

Defined in <`stapl/containers/graph/algorithms/maximal_bipartite_matching.hpp`>

```c++
template<class GView>  
size_t maximal_bipartite_matching(GView & g,size_t k)
```

## Summary

A Parallel Maximal Bipartite Matching (MBM)

Performs a maximal bipartite matching on the input graph_view, storing the matchings on each vertex. 
#### Parameters
* *g*: The graph_view over the input graph. The input must be a bipartite graph, with the vertices divided in one of properties::bipartite_partition::LEFT or properties::bipartite_partition::RIGHT, as specified by the vertex property. Edges should only occur between vertices of different partitions, i.e., inter-partition. 





#### Returns
The size of the maximal matching.
