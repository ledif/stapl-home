# triangle_count

Defined in <`stapl/containers/graph/algorithms/triangle_count.hpp`>

```c++
template<typename GView>  
std::pair< size_t, double > triangle_count(GView & graph,size_t k)
```

## Summary

Parallel Triangle Counting and Clustering Coefficient Algorithm.

Counts the number of triangles in the input graph_view, storing partial counts on vertices that are part of triangles. Also computes the clustering-coefficient of the input graph_view, storing counts of connected triplets on vertices. Formula: C(G) = 3x#triangles / #connected-triplets-of-vertices. = #closed-triplets / #connected-triplets-of-vertices.


#### Parameters
* *graph*: The graph_view over the input graph. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous paradigm. k >= D implies fully asynchronous paradigm (D is diameter of graph). 





#### Returns
pair of the number of triangles detected and the clustering-coefficient of the graph.
