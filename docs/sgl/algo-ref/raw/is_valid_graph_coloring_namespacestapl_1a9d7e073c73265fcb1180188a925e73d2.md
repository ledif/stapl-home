# is_valid_graph_coloring

Defined in <`stapl/containers/graph/algorithms/graph_coloring.hpp`>

```c++
template<typename GView,typename ColorMap>  
bool is_valid_graph_coloring(GView const & graph,ColorMap & color_map)
```

## Summary

Computes if the given coloring is valid for the input graph.

#### Parameters
* *graph*: The graph_view over the input graph. 


* *color_map*: The input vertex property map where the color information is stored. [vertex->color (int)]. Must have been previously populated by calling [color_graph()](#namespacestapl_1a7ef86fe8b7e7af4d903f22f940d17a1b). 





#### Returns
True if the coloring is valid, false otherwise.
