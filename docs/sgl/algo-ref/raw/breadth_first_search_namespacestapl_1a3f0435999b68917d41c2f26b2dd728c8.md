# breadth_first_search

Defined in <`stapl/containers/graph/algorithms/breadth_first_search.hpp`>

```c++
template<class GView>  
size_t breadth_first_search(GView & g,typename GView::vertex_descriptor const & source,size_t k)
```

## Summary

A Parallel Breadth-First Search (BFS)

Performs a breadth-first search on the input graph_view, storing the BFS-level and BFS-parent on each reachable vertex. 
#### Parameters
* *g*: The graph_view over the input graph. 


* *source*: The descriptor of the source vertex for this traversal. 


* *k*: The maximum amount of asynchrony allowed in each phase. 0 <= k <= inf. k == 0 implies level-synchronous BFS. k >= D implies fully asynchronous BFS (D is diameter of graph). 





#### Returns
The number of iterations performed by the paradigm.


**See also**: [properties::bfs_property](#classstapl_1_1properties_1_1bfs__property)

#### Example
```cpp
  using property_type = stapl::properties::bfs_property;
  using graph_type = stapl::multidigraph<property_type>;

  auto g = stapl::read_edge_list<graph_type>(argv[1]);

  stapl::breadth_first_search(g, 0, 10);

  std::size_t hops = g[10].property().level();

  std::cout << "vertex 10 is " << hops << " hops away from vertex 0";
```
