# sharded_graph_reader

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename Graph,typename LineReader>  
graph_view< Graph > sharded_graph_reader(std::string filename,LineReader line_reader)
```

## Summary

Algorithm to read a sharded graph from the given input file.

Uses the user-provided reader to read each line and add the edges from the line to the graph. Input graph must be sharded using the sharder. A sharded file is split into multiple input files and a metadata file that keeps track of the number of shards, vertices and edges, etc.

#### Parameters
* *filename*: The name of the input file. 


* *line_reader*: The functor used to read-in a line of text from file and add the appropriate edges to the provided aggregator, LineReader is given a stream with the line and an Edge-Aggregator. It should read the stream and add the appropriate edges to the provided Aggregator. LineReader should return '0' for success, '1' for failure. 





#### Returns
A graph_view over the output graph, populated with the vertices and edges from the input file.
