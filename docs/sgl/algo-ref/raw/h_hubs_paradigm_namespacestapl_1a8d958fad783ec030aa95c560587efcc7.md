# h_hubs_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/h_hubs_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename VPR,typename PostExecute,typename HView,typename HubsView,typename GView>  
size_t h_hubs_paradigm(WF const & uwf,UF const &,VPR const & vpr,PostExecute post_execute,HView & h,HubsView & hubs,GView & g)
```

## Summary

The Parallel Hierarchical Hubs Level-Synchronous (h-hubs) Paradigm.

Implements the hierarchical hubs level-sync paradigm, which iteratively executes BSP-Supersteps (SS). Each SS applies the user provided vertex-operator over the vertices of the input graph. Any communication generated within a SS is guaranteed to have finished before the SS finishes. The communication happens hierarchically, by following the locality of the graph through the machine-hierarchy and the hubs-hierarchy. Updates (neighbor-operators) to be applied to remote neighbors of a vertex v are sent to v's parent vertex in the machine hierarchy. The parent then forwards the update to all its neighbors that contain neighbors of v. When the update is received by the parent's neighbor, it applies the update to all of its children that are neighbors of v in the lower-level graph. The update is also sent to v's parent vertex in its hub-hierarchy. The hub-hierarchy contains representatives of high-degree vertices (hubs) on each location, created using [create_level_hubs()](#namespacestapl_1a71fbd54c2a39438d6615da4bbb7d8c11). The update is applied to the hub-representatives of all neighboring hub-vertices of vertex v. Upon completion of an SS, the hub-representatives on each location are flushed to the actual hubs vertices in the lower-level graph, using the provided reduction operator (`vpr`). Finally, the local neighbors of v are updated. This allows us to send a single update per remote location vs. sending an update per remote/cross edge, as well as avoid heavy communication to hub-vertices. This may lead to a reduction in the amount of communication from O(E) to O(V), providing faster and more scalable performance.

The hierarchical hubs paradigm is an extension of the hierarchical paradigm ([h_paradigm()](#namespacestapl_1ab2f2aaf9b13ecd411395eae6d992804b)), and adds the ability to reduce communication to hub vertices of the graph. The hierarchical paradigm reduces outgoing communication from vertices, while the hubs-hierarchy allows the reduction of incoming communication to high-degree vertices.

The user provides a vertex-operator to express the computation to be performed on each vertex, and a neighbor-operator that will be applied to each neighbor that is visited. The vertex-operator is passed in a vertex and a visit object. To visit a neighboring vertex, the vertex-operator must call visit_all_edges(neighbor, visitor()). The vertex-operator must return true if the vertex was active (i.e. its value was updated), or false otherwise. The neighbor-operator is passed in the target vertex. Neighbor-operators may carry state, but must be immutable. They should return true if the visit was successful (i.e. the target vertex will be activated after this visit), or false otherwise. Users may also provide additional functions to be executed before and after each SS. 
#### Parameters
* *WF*: The type of the user provided vertex-operator expressing computation to be performed over each vertex. 


* *UF*: The type of the user provided neighbor-operator expressing computation to be performed over neighboring vertices. 





#### Parameters
* *vpr*: A vertex property reducer for the algorithm. It should accept two vertex properties and reduce them to update the first one. Used to update the hub vertices. 


* *post_execute*: Optional functor that will be executed on the graph_view at the end of each SS. This will be invoked with the input graph_view and the current SS ID (the ID of the SS that just finished). 


* *h*: The hierarchical machine graph_view over the input graph. This must be created using [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb). 


* *hubs*: The hierarchical hubs graph_view over the input graph. This must be created using [create_level_hubs()](#namespacestapl_1a71fbd54c2a39438d6615da4bbb7d8c11) over the input graph, before calling [create_level_machine()](#namespacestapl_1a642f8180b57c54c687fdccf799ec05bb). 


* *g*: The graph_view over the input graph. 





#### Returns
The number of iterations performed by the paradigm.
