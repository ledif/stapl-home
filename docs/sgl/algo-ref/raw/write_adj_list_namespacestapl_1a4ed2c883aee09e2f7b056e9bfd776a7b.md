# write_adj_list

Defined in <`stapl/containers/graph/algorithms/graph_io.hpp`>

```c++
template<typename GraphVw>  
void write_adj_list(GraphVw & g,std::string filename)
```

## Summary

Function to output the adjacency-list of a graph.

Format: /// VID VERTEXPROP #edges TGT0 EDGEPROP0 TGT1 EDGEPROP1 ...
/// VID VERTEXPROP #edges TGT0 EDGEPROP0 TGT1 EDGEPROP1 ...
/// :
/// 
#### Parameters
* *g*: The input graph_view to be printed. 


* *filename*: The name of the file where the graph will be printed.
