# closeness_centrality

Defined in <`stapl/containers/graph/algorithms/closeness_centrality.hpp`>

```c++
template<typename GView>  
void closeness_centrality(GView & graph,size_t k)
```

## Summary

Parallel Closeness Centrality Algorithm.

Calculates the closeness-centrality of each vertex in a graph. Closeness(v) = 1 / Farness(v) = 1 / Sum(d(u,v)), where d(u,v) is the shortest-distance from every vertex u in the graph to the given vertex v. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *k*: The level of asynchrony in the algorithm.
