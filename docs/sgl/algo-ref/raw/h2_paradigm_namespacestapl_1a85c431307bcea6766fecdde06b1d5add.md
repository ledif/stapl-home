# h2_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/h2_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename H2View,typename H1View,typename GView>  
size_t h2_paradigm(WF const & uwf,UF const &,H2View & h2,H1View & h1,GView & g,size_t aggr_sz)
```

## Summary

The Parallel 2-level Hierarchical Level-Synchronous (h2) Paradigm.

Overloaded variant of [h2_paradigm()](#namespacestapl_1a1ee8033c45c79a5b9ac8b34ac3351267) with an empty post-execute.
