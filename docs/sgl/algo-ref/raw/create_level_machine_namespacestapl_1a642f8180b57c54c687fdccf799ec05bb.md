# create_level_machine

Defined in <`stapl/containers/graph/algorithms/create_level_machine.hpp`>

```c++
template<typename GraphView>  
graph_view< digraph< [create_level_machine_detail::super_vertex_machine_property](#structstapl_1_1create__level__machine__detail_1_1super__vertex__machine__property)< GraphView > > > create_level_machine(GraphView & gvw,bool delete_edges,size_t max_msg_sz)
```

## Summary

Algorithm to create a level of hierarchy based on the machine hierarchy, for use in [h_paradigm()](#namespacestapl_1ab2f2aaf9b13ecd411395eae6d992804b).

Creates a level of hierarchy based on the input graph_view and vertex-grouping by locality, i.e., all vertices on a location belong to the same supervertex, and each location has one supervertex. Supervertices and superedges in the resulting output graph store descriptors of their children in the input. 
#### Parameters
* *gvw*: The input graph_view over which the new level is to be created. 


* *max_msg_sz*: The number of requests to aggregate for the aggregator. 


* *delete_edges*: Indicates if the non-local edges of the input graph will be deleted. This is true by default, as the [h_paradigm](#namespacestapl_1ab2f2aaf9b13ecd411395eae6d992804b) requires the edges to be deleted. 





#### Returns
A graph_view over the output supergraph. 

Destructive, the input graph view will be mutated to delete some or all edges, but the information of the graph is maintained between the input and the hierarchy.
