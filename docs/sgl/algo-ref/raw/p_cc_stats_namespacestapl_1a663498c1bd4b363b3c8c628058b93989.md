# p_cc_stats

Defined in <`stapl/containers/graph/algorithms/connected_components_hierarchical.hpp`>

```c++
template<typename View>  
void p_cc_stats(View & g)
```

## Summary

Given a graph, mark all vertices belonging to the same connected component in a property map.

#### Parameters
* *g*: A view of the graph. 





> Todo: This should take a property map as parameter and populate it, instead of assuming the vertex has the necessary property interface.
