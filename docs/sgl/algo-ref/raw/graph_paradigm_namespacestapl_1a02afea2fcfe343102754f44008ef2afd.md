# graph_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/graph_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename GView>  
size_t graph_paradigm(WF const & uwf,UF const &,GView & g,int k)
```

## Summary

The Parallel Graph Algorithm Paradigm.

Overloaded variant of [graph_paradigm](#namespacestapl_1a7f07bfdd1121365449ad424155af5d1c) with an empty post-execute.
