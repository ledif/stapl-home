# kla_paradigm

Defined in <`stapl/containers/graph/algorithms/paradigms/kla_paradigm.hpp`>

```c++
template<typename WF,typename UF,typename GView,typename Predicate>  
size_t kla_paradigm(WF const & uwf,UF const &,GView & g,size_t k,[kla_params](#structstapl_1_1kla__params)< GView, Predicate > const & params)
```

## Summary

The Parallel k-Level-Asynchronous (KLA) Paradigm.

Overload of [kla_paradigm](#namespacestapl_1ad760156f246f343255ae7691cd5c4a01) with an empty post-execute.
