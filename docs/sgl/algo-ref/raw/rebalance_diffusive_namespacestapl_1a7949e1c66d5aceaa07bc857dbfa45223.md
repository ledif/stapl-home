# rebalance_diffusive

Defined in <`stapl/containers/graph/algorithms/rebalance_diffusive.hpp`>

```c++
template<typename C,typename W>  
void rebalance_diffusive(C & v,W const & weight_map)
```

## Summary

Rebalance a graph diffusively by iteratively moving vertices between neighboring partitions.

Balances the graph based on vertex-weights by migrating vertices to create an even weight-distribution among the locations, while attempting to minimize total movement of vertices.


#### Parameters
* *v*: The graph_view over the input graph. 


* *weight_map*: The vertex property map storing weights for each vertex. 




Invalidates the input graph_view.
