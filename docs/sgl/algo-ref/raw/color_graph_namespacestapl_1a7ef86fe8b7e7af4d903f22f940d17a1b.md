# color_graph

Defined in <`stapl/containers/graph/algorithms/graph_coloring.hpp`>

```c++
template<template< typename, typename, typename, typename > class GView,template< graph_attributes, graph_attributes, typename...> class Graph,graph_attributes M,typename... OptionalParams,typename Dom,typename MapFunc,typename Derived,typename ColorMap>  
void color_graph(GView< Graph< DIRECTED, M, OptionalParams...>, Dom, MapFunc, Derived > const & graph,ColorMap & color_map)
```

## Summary

Parallel Level-Synchronized Graph Coloring Algorithm.

Assigns coloring to the vertices of the input graph, such that neighboring vertices are not assigned the same color. 
#### Parameters
* *graph*: The graph_view over the input graph. 


* *color_map*: The output vertex property map where the color information will be stored. [vertex->color (int)] Specialization for directed graphs. Copies the directed graph to an undirected graph and computes coloring for the undirected graph. 




This method invalidates the input graph view.


> Todo: This could be using an undirected_view over the input graph instead of creating its own methods to make the undirected graph.
